//
//  SearchResultsViewController.swift
//  CoreDataTest
//
//  Created by MacStudent on 2018-11-12.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CoreData

class SearchResultsViewController: UIViewController {

    var nameToSearch:String = "";
    var context:NSManagedObjectContext!
    
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var passwordlbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup your CoreData variable
        // ----------------------------------------
        
        // 1. Mandatory - copy and paste this
        // Explanation: try to create/initalize the appDelegate variable.
        // If creation fails, then quit the app
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext

        print("loaded 2 screen");
        print("Person Name: \(self.nameToSearch)");
        
        
        
        // This is the same as:  SELECT * FROM User
        let fetchRequest:NSFetchRequest<User> = User.fetchRequest()
        
        //WHERE
        fetchRequest.predicate = NSPredicate(format: "email == %@", self.nameToSearch);
        
        
        do {
            // Send the "SELECT *" to the database
            //  results = variable that stores any "rows" that come back from the db
            // Note: The database will send back an array of User objects
            // (this is why I explicilty cast results as [User]
            let results = try self.context.fetch(fetchRequest) as [User]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            if results.count == 1 {
                let x = results[0] as User;
                namelbl.text = x.email;
                passwordlbl.text = x.password;
            }
        }
        catch {
            print("Error when fetching from database")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
