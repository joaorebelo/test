//
//  EditUserViewController.swift
//  CoreDataTest
//
//  Created by MacStudent on 2018-11-12.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CoreData

class EditUserViewController: UIViewController {
    
    //placeholder holder
    var person:User!;
    

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var emaillbl: UILabel!
    @IBOutlet weak var passwordlbl: UILabel!
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var passwordtxt: UITextField!
    @IBAction func saveChangesbtn(_ sender: Any) {
        person.email = emailtxt.text;
        person.password = passwordtxt.text;
        do {
            // Save the user to the database
            // (Send the INSERT to the database)
            print("saved to database")
            try self.context.save()
        }
        catch {
            print("Error while saving to database")
        }
    }
    
    var context:NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup your CoreData variable
        // ----------------------------------------
        
        // 1. Mandatory - copy and paste this
        // Explanation: try to create/initalize the appDelegate variable.
        // If creation fails, then quit the app
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext
        
        print("Loaded edit user screen")
        print("person: \(person.email!) and \(person.password!)")
        emailtxt.text = person.email!;
        passwordtxt.text = person.password!;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
